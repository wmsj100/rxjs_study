import { Component, OnInit } from '@angular/core';
import { NgModel, FormsModule } from '@angular/forms';
import {
  fromEvent, Observable, Subject, BehaviorSubject,
  ReplaySubject, AsyncSubject, from, range, of,
  interval, combineLatest, concat, merge, zip, timer
} from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { scan, throttleTime, map, filter, take, delay, max, min, reduce, sampleTime, debounceTime } from 'rxjs/operators';
import { Button } from '../../../node_modules/protractor';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styleUrls: ['./rxjs.component.css']
})
export class RxjsComponent implements OnInit {
  inputVal = 'test1';
  observer = { next: data => console.log(data) };
  constructor(
  ) {
  }
  ngOnInit() {
    // this.getClientx();
    // this.nextDemo1();
    // this.asyncDemo1();
    // this.clearObservable();
    // this.unsubscribeDemo1();
    // this.subjectDemo1();
    // this.moreBo();
    // this.replaySubjectDemo1();
    // this.replaySubjectDemo2();
    // this.asyncSubjectDemo1();
    // this.producerDemo1();
    // this.operatorsDemo1();
    // this.controllerDemo1();
    // this.ajaxDemo1();
    // this.intervalDemo1();
    // this.concatDemo1();
    // this.mergeDemo1();
    // this.zipDemo1();
    // this.numberDemo1();
    // this.intervalDemo2();
    // this.timerDeme1();
    // this.intervalDemo3();
    // this.sampleTimeDemo1();
    this.debounceTimeDemo1();
  }

  debounceTimeDemo1() {
    const input = document.querySelector('input');
    fromEvent(input, 'keyup').pipe(
      map(i => i.currentTarget['value']),
      debounceTime(1000)
    ).subscribe(this.observer);
  }

  sampleTimeDemo1() {
    const btn = document.querySelector('button');
    const start = +new Date();
    fromEvent(btn, 'click').pipe(
      sampleTime(2000)
    ).subscribe(val => {
      console.log(val, +new Date() - start);
    });
  }

  intervalDemo3() {
    interval(500).pipe(
      take(3),
      delay(300)
    ).subscribe(this.observer);
  }

  timerDeme1() {
    timer(1000).subscribe(this.observer);
    timer(2000, 500).pipe(take(3)).subscribe(this.observer); // 间隔2s生成0,然后每隔500ms生成1,2,然后停止
  }

  intervalDemo2() {
    interval(100).pipe(take(3)).subscribe(this.observer); // 0,1,2
  }

  numberDemo1() {
    of(1, 4, 2, 3).pipe(max()).subscribe(this.observer);  // 4
    of(1, 4, 2, 3).pipe(min()).subscribe(this.observer);  // 1
    of(1, 4, 2, 3).pipe(
      reduce((accumulated, current) => accumulated + current))
      .subscribe(this.observer);  // 10
    of({ name: 'wmsj100' }, { age: 12 }).pipe(
      reduce((acc, curr) => Object.assign({}, acc, curr))
    )
      .subscribe(this.observer); // { name: "wmsj100", age: 12 }
  }

  zipDemo1() {
    const zip$ = zip(
      of(1),
      of(2, 3, 4),
      of(7)
    ).subscribe(this.observer);

    zip(
      of(2, 4),
      of(6, 3, 5),
      of(8, 9)
    ).subscribe(this.observer);
  }

  mergeDemo1() {
    const merged$ = merge(
      of(1).pipe(delay(500)),
      of(3, 2, 5)
    );
    const observer = { next: data => console.log(data) };
    merged$.subscribe(observer);
  }

  concatDemo1() {
    const source1 = interval(100).pipe(
      map(val => 'sourc3e1 ' + val),
      take(5)
    );
    const source2 = interval(50).pipe(
      map(val => 'source2 ' + val),
      take(3)
    );
    const stream$ = concat(source1, source2);
    stream$.subscribe(data => console.log(data));
  }

  intervalDemo1() {
    const source1 = interval(100).pipe(
      map(val => 'sourc3e1 ' + val),
      take(5)
    );
    const source2 = interval(50).pipe(
      map(val => 'source2 ' + val),
      take(3)
    );
    const stream$ = combineLatest(source1, source2);
    stream$.subscribe(data => console.log(data));
  }

  ajaxDemo1() {
    const that = this;
    const people$ = ajax({
      url: 'https://swapi.co/api/people/1',
      crossDomain: true,
      createXHR: () => {
        return new XMLHttpRequest();
      }
    })
      .pipe(
        map(e => e.response)
      )
      .subscribe(res => {
        that.inputVal = res.name;
        console.log(res);
      });


  }

  controllerDemo1() {
    of(1, 2, 3, 4).pipe(
      filter(val => {
        return val % 2 === 0;
      })
    ).subscribe(x => console.log(x));
  }

  operatorsDemo1() {
    const source$ = range(0, 10);
    source$.pipe(
      filter(x => x % 2 === 0),
      map(x => x + x),
      scan((acc, x) => acc + x, 0)
    ).subscribe(x => console.log(x));
  }

  producerDemo1() {
    from([1, 2, 3]).subscribe((val) => console.log(val));
  }

  asyncSubjectDemo1() {
    const subject = new AsyncSubject();
    subject.subscribe({
      next: (v) => console.log('observerA: ', v)
    });
    subject.next(1);
    subject.next(2);
    subject.next(3);
    subject.next(4);
    subject.subscribe({
      next: (v) => console.log('observerB: ', v)
    });
    subject.next(5);
    subject.complete();
  }

  replaySubjectDemo2() {
    const subject = new ReplaySubject(100, 500);
    const sub1 = subject.subscribe({
      next: (v) => console.log('observerA: ', v)
    });
    let i = 1;
    setInterval(() => subject.next(i++), 200);
    setTimeout(() => {
      const sub2 = subject.subscribe({
        next: (v) => console.log('observerB: ', v)
      });
      sub1.add(sub2);
    }, 1000);
    setTimeout(() => {
      sub1.unsubscribe();
    }, 5000);
  }

  replaySubjectDemo1() {
    const subject = new ReplaySubject(3);

    subject.subscribe({
      next: (v) => console.log('objectA: ', v)
    });

    subject.next(1);
    subject.next(2);
    subject.next(3);
    subject.next(4);
    subject.subscribe({
      next: (v) => console.log('observerB: ', v)
    });
    subject.next(5);
  }

  moreBo() {
    const subject = new BehaviorSubject(0);
    subject.subscribe({
      next: (v) => console.log('observerA: ', v)
    });
    subject.next(1);
    subject.next(2);
    subject.subscribe({
      next: (v) => console.log('observerB: ', v)
    });

    subject.next(3);
  }

  subjectDemo1() {
    const subject = new Subject();
    subject.subscribe({
      next: (v) => console.log('objectA: ', v)
    });

    subject.subscribe({
      next: (v) => console.log('observerB: ', v)
    });

    subject.next(1);
    subject.next(2);

  }

  unsubscribeDemo1() {
    const observable1 = Observable.create((observer) => {
      let i = 0;
      setInterval(() => {
        observer.next(i++);
      }, 400);
    }).subscribe(x => console.log('name1: ', x));
    const observable2 = Observable.create((observer) => {
      let i = 0;
      setInterval(() => {
        observer.next(i++);
      }, 300);
    }).subscribe(x => console.log('name2: ', x));
    observable1.add(observable2);
    setTimeout(() => {
      observable1.unsubscribe();
    }, 1000);
  }

  clearObservable() {
    const observable1 = Observable.create((observer) => {
      const interId = setInterval(() => {
        observer.next('hi');
      }, 1000);

      // return function unsubscribe() {
      //   clearInterval(interId);
      // };
    });
    observable1.subscribe(x => console.log(x)).unsubscribe();
  }

  asyncDemo1() {
    const foo = Observable.create((observer) => {
      console.log('Hello');
      observer.next(42);
    });

    foo.subscribe(x => console.log(x));
    foo.subscribe(y => console.log(y));

  }

  nextDemo1() {
    const observable2 = Observable.create((observer) => {
      observer.next(1);
      observer.next(2);
      observer.next(3);
      setTimeout(() => {
        observer.next(4);
      }, 1000);
    });
    console.log('just before subscribe');
    observable2.subscribe({
      next: x => console.log(`got value ${x}`),
      error: err => console.error(`something wrong occurred: ${err}`),
      complete: () => console.log('done')
    });
    console.log('just after subscribe');
  }

  getClientx() {
    const button = document.querySelector('button');
    const stream$ = fromEvent(button, 'click').pipe(
      throttleTime(1000),
      map(event => event['clientX']),
      scan((count, clientX) => count + clientX, 0)
    )
      .subscribe(count => console.log(count));

  }

}
